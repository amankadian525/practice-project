resource "google_compute_instance" "linux_vm" {

    name = var.vm_name
    machine_type = var.machine_type
    zone = var.vm_zone
    tags = var.network_tags
    hostname = var.vm_hostname
    boot_disk {
        image = var.image
        size = var.size
        type = var.type
    }

    attched_disk {
        source = var.vm_source
        device_name = var.vm_device_name
    }

    network_interface {
        subnetwork = var.vm_subnetwork
        network = var.vm_network
    }
      service_account {
    email  = var.vm_service_account
    scopes = var.vm_scopes
  }
}
