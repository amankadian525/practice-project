output "service_account_id" {
    value = var.service_account_id
}

output "service_account_name" {
    value = var.service_account_display_name
}

output "service_account_description" {
    value = var.service_account_description
}

